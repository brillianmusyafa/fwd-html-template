(function($) {
    $(function() {

        $('.sidenav').sidenav();

    }); // end of document ready
})(jQuery); // end of jQuery name space

var instance = M.Carousel.init({
    fullWidth: true,
    indicators: true
});

// Or with jQuery

$('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
});


// forms input:select

$(document).ready(function() {
    $('select').formSelect();
});


// modals

$(document).ready(function() {
    $('.modal').modal();
});


// autocomplete
// Or with jQuery

$(document).ready(function() {
    $('input.autocomplete').autocomplete({
        data: {
            "Apple": null,
            "Microsoft": null,
            "Google": 'https://placehold.it/250x250'
        },
    });
});



$(document).ready(function() {
    $('.collapsible').collapsible();
});